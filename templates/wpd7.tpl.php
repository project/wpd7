<?php

/**
 * @file
 * Web Profiler block - Template file.
 */
?>

<section class="wpd7-template">
  <!-- Print block title -->
  <?php print render($title_prefix); ?>
  <?php if ($title): ?>
    <h2<?php print $title_attributes; ?>><?php print $title; ?></h2>
  <?php endif;?>
  <?php print render($title_suffix); ?>


  <!-- Block content -->
  <div class="wpd7-block">  
    <div class="asset-wrapper"></div>
    <div class="database-wrapper"></div>
    <div class="form-wrapper"></div>
    <div class="user-wrapper"></div>
    <div class="views-wrapper"></div>
  </div>



<!-- <?php print render($variables['user_image']); ?> -->
</section>