<?php

/**
 * @file
 * Data Collector interface.
 */

namespace Drupal\wpd7\Common;


/**
 * Data Collector interface.
 */
interface DataCollectorInterface {

  /**
   * Returns the datacollector title.
   * @return string
   * 
   */
  public function getTitle();

  /**
   * Returns the collector icon in base64 format.
   * @return string
   */
  public function getIcon();


  /**
   * @return array
   */
  public function getData();

}