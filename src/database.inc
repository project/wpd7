<?php

// Declare class usage.
use Drupal\wpd7\Common\DataCollectorInterface;


/**
 * Class AssetDataCollector
 */
class DatabaseDataCollector implements DataCollectorInterface {

	private $data;

	public function __construct($data) {
		$this->data = $data;

	}

	/**
	 * {@inheritdoc}
	 */
	public function getTitle() {
		return $this->t('Database');
	}


	/**
	 * {@inheritdoc}
	 */
	public function getIcon() {
		return 'sfsfsdfsfsdsdfs';
	}

	/**
	 * {@inheritdoc}
	 */
	public function getData() {

		$data['queries_time'] = $queries_time;
		$data['db_queries'] = $db_queries;
		$data['memory_amount'] = $memory_amount;

		return $data;
	}

}